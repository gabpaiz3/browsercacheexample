var moment = require('moment');
var express = require('express');
var app = express();
var port = 8088;

app.use(express.static('public'));
app.set('etag', false);

app.locals.pretty = true;
app.set('json spaces', 2);

app.use('/data', function(req, res, next) {
    res.removeHeader('Cache-Control');
    res.set({
        'Cache-Control': 'private, max-age=60',
        'Vary': 'Accept, Accept-Encoding, Authorization, dealershipId'
    });
    next();
});

app.get('/data', function(req, res) {
    res.json({created: moment().format()});
});

app.listen(port, function() {
    console.log('listening on ' + port);
});