# README Browser Caching POC #

### What is this repository for? ###

* Express and Angular example of caching data responses in the browser from REST services
* Version 1
* How to run

    1. Do Setup instructions below
    2. Run node app.js start
    3. Access http://localhost:8088/index.html

This example illustrates use of Vary and Cache-Control headers to have browsers cache responses from data calls.  These headers are used:


```
#!javascript

    res.removeHeader('Cache-Control');
    res.set({
        'Cache-Control': 'private, max-age=60',
        'Vary': 'Accept, Accept-Encoding, Authorization, dealershipId'
    });

```

To tell browser to cache this data output for one minute.  This can be demonstrated by running example and then clicking the Get Data button repeatedly.  NOTE: having developer tools window open disables caching for requests, so ensure any of those windows are closed.

The Vary header tells the browser when to re-get data based on request headers.  Changing the Authorization header will result in the browser invalidating the previously cached data and making a new request.  Any header can be included in the Vary list.

The other very important aspect of this example is which headers are not sent.  Using Vary and max-age to enforce a time-to-live (TTL) cache requires that no validators be sent.  These include:

* Etag
* Last-Modified

The following code ensures that Express does not send etags:

```
#!javascript

app.set('etag', false);
```

Express appears to not set Last-Modified by default, so there's no code to address that.


This example uses Node and Express to illustrate the use of the headers.  The same can be done quite easily from .Net Web.Api projects using a FilterAttribute.


### How do I get set up? ###

* Setup

    1.  Get repo
    2.  Run npm install
    3.  Run node app.js start

* Configuration

    1. Update port in appSettings.js (TBA)

* Dependencies

    1. Express
    2. Moment
    3. Angular (references official googleapis)

* Deployment instructions

    1. Run node deploy.js from Octopus PowerShell (TBA)

* Testing instructions

    1. Run node test.js from Octopus PowerShell (TBA)
	
* 	

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner: gpaiz@dealersocket.com